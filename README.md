# Personal Data Related Project

## Description
This Project focuses on applying data analytics and database knowledge into creating portfolio for future advancedment.

## Project 1 (Empowering Stock Market Insights through Power BI)
Leveraging the capability of Power BI, I am meticulously crafting a simple stock market database. This innovative database provide latest feeds from Bursa Malaysia, focusing on pivotal performance indicators, precise stock valuations, and an array of other pertinent metrics. The results becomes an invaluable compass for comprehensive decision-making, giving the best outcome for financial success.

## Project 2 (Stock Market Dashboard using Google Sheets)
Develop a comprehensive dashboard in Google Sheets that aggregates real-time data from Bursa Malaysia (Malaysian stock market). The dashboard include key performance indicators, stock prices, and other relevant metrics for informed decision-making.

## Project 3 (Data Visualization for Stock Market with Power BI)
Create a data visualization project using Microsoft Power BI to present stock market data in an interactive and insightful manner. This include trend analysis, stock price fluctuations and trends, helping users to make informed investment choices.

## Project 4 (Google Sheet Template for Portfolio Analysis with KLCI and ASB)
Design a Google Sheet template that enables investors to track and analyze the performance of their investment portfolio, comparing it against the KLCI index and ASB (Amanah Saham Bumiputera) returns. The template should offer insights into portfolio diversification, risk assessment, and potential adjustments.

## Project 5 (Google Sheet Template for DCF Analysis on Malaysian Stocks)
Develop a Google Sheet template that simplifies the Discounted Cash Flow (DCF) analysis process for evaluating Malaysian stocks. Users should be able to input their preferred assumptions and parameters to calculate the intrinsic value of stocks based on their personal criteria.

## Project 6 (ASB and KWSP Savings Return Calculator using Google Sheets)
Create a calculator in Google Sheets that calculates the projected returns from investments in Amanah Saham Bumiputera (ASB) and the Employees Provident Fund (KWSP) based on the latest return rates. Visualize the potential growth of savings over time to help individuals plan for their financial goals.

## Support
Connect with me via https://www.linkedin.com/in/muhamad-aliff-haikal-biamin-b37284204/






